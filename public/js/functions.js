(function( $ ) {
    $(document).ready(function (){
        let people = document.getElementsByClassName("count_people");

        // Numero de comensales
        $(document).on('click','.entry-icon-minus',function(event){
            event.preventDefault();
            let value = people[0].innerHTML;
            let count = Math.round(value.charAt(0)) - 1;
            if(count >= 1) {
                people[0].innerHTML = count + ' people';
            }
        });
        $(document).on('click','.entry-icon-plus',function(event){
            event.preventDefault();
            let value = people[0].innerHTML;
            let count = Math.round(value.charAt(0)) + 1;
            if(count <= 8) {
                people[0].innerHTML = count + ' people';
            }
        });

        // Carrousel
        var slideIndex = 0;
        showDivs(slideIndex);

        function showDivs(n) {
        var i;
        var x = document.getElementsByClassName("entry-event");
        if (x.length) {
            for (i = 0; i < x.length; i++) {
                if(x[i].classList.contains("event-active")){
                    x[i].classList.remove("event-active");
                }
            }
            x[n].className += " event-active";
            }
    
            $(document).on('click','.family',function(){
                showDivs(0);
            }),
            $(document).on('click','.special',function(){
                showDivs(1);
            }),
            $(document).on('click','.social',function(){
                showDivs(2);
            })
        }

        function getElement(value){
            return document.getElementsByClassName(value)[0].value;
        }

        // Comprobar formulario
        $(document).on('click','.reservation-button',function(event){
            event.preventDefault();
            const name = getElement("form-name");
            const email = getElement("form-email");
            const month = getElement("form-month");
            const day = getElement("form-day");
            const year = getElement("form-year");
            const hour = getElement("form-hour");
            const min = getElement("form-min");
            
            const isNameError = checkEmptyInput(name);
            const isEmailError= checkEmptyInput(email);
            const isDateError = checkDate(month, day, year);
            const isTimeError = checkTime(hour, min);

            const nameError = document.getElementsByClassName("li-name");
            const emailError = document.getElementsByClassName("li-email");
            const dateError = document.getElementsByClassName("li-date");
            const timeError = document.getElementsByClassName("li-time");

            if (isNameError) {
                nameError[0].lastElementChild.style.display="flex";
                if(!nameError[0].classList.contains("input-error")){
                    nameError[0].className += " input-error";
                }
            } else {
                nameError[0].lastElementChild.style.display="none";
                nameError[0].classList.remove('input-error');
            }
            if (isEmailError) {
                emailError[0].lastElementChild.style.display="flex";
                if(!emailError[0].classList.contains("input-error")){
                    emailError[0].className += " input-error";
                }
            } else {
                emailError[0].lastElementChild.style.display="none";
                emailError[0].classList.remove('input-error');
            }
            if (isDateError) {
                dateError[0].lastElementChild.style.display="flex";
                const input_childs = dateError[0].querySelectorAll('input');
                for (let child of input_childs) {
                    if(!child.classList.contains("input-error")){
                        child.className += " input-error";
                    }
                }
            } else {
                dateError[0].lastElementChild.style.display="none";
                const input_childs = dateError[0].querySelectorAll('input');
                for (let child of input_childs) {
                    child.className += " input-error";
                    child.classList.remove('input-error');
                }
            }
            if (isTimeError) {
                timeError[0].lastElementChild.style.display="flex";
                const input_childs = timeError[0].querySelectorAll('input');
                for (let child of input_childs) {
                    if(!child.classList.contains("input-error")){
                        child.className += " input-error";
                    }
                }
            } else {
                timeError[0].lastElementChild.style.display="none";
                const input_childs = timeError[0].querySelectorAll('input');
                for (let child of input_childs) {
                    child.className += " input-error";
                    child.classList.remove('input-error');
                }
            }
        })

        function checkEmptyInput(value){
            if(value){
                return false
            }
            return true
        }
        function checkDate(month, day, year){
            const booking_date = new Date(year,month,day);
            const fecha = new Date();
            const añoActual = fecha.getFullYear();
            const mesActual = fecha.getMonth() + 1; 
            const hoy = fecha.getDate();
            const today = new Date(añoActual,mesActual,hoy);
            const arrayMonth = [31,29,31,30,31,30,31,31,30,31,30,31];
            if (month>0 && month<=12 && day>=0 && day<=arrayMonth[month-1] && year>=2023){
                if(booking_date >= today){
                    return false
                }
            }
            return true
        }
        function checkTime(hour, min){
            if(hour>=1 && hour<=12 && min>=0 && min<=59){
                return false
            }
            return true
        }
    })
})( jQuery );